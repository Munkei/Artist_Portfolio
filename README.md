# You need to have Ruby installed on your machine

## For Mac

```bash
sudo apt-get install ruby
or
brew install ruby
```

&nbsp;

## For Windows

- Use the Rubyinstaller to install. LINK: https://www.ruby-lang.org/en/documentation/installation/#rubyinstaller

&nbsp;


## For Linux/UNIX

- Go to the git repository and follow furthur instructions: https://github.com/rbenv/rbenv

&nbsp;

# Configuration

Execute the following bash command in your terminal screen.

```bash
sass --watch scss:css
```

Now you should see "Sass is watching for changes" that indicates that Sass is working. Make sure you have SCSS running before making any changes to a CSS file.

&nbsp;

> Note: All changes made in the css folder will be deleted, so the changes must be in the scss folder.