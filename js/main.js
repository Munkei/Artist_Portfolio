;(function () {
    let nav = document.querySelector('[data-role=nav]');
    let burgerMenu = document.querySelector('[data-role=burger]');
    let header = document.querySelector('[data-role=header]');

    const openOrCloseNav = function openOrCloseNav() {
        if(nav.classList.contains('open')) {
            nav.classList.remove('open');
            burgerMenu.classList.remove('open');
        } else {
            nav.classList.add('open');
            burgerMenu.classList.add('open');
        }
    }

    const scrollTo = function scrollTo(idName) {
        let ele = document.querySelector('#' + idName);
        let top = ele.offsetTop - header.offsetHeight;
        window.scrollTo ({ 
            top: top,
            left: 0, 
            behavior: "smooth" 
        });
        openOrCloseNav();
    }

    window.onscroll = function (e) {  
        if (document.body.scrollTop >= 60) {
            header.classList.add('open');
        } else {
            header.classList.remove('open');
        }
    }
    
    burgerMenu.addEventListener('click', openOrCloseNav);

    nav.addEventListener('click', function(e) {
        idName = e.target.innerHTML;
        idName = idName.toLowerCase();
        idName = idName.replace(' ', '-');
        scrollTo(idName);
    });
})();